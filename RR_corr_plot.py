#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28

@author: michaelodonnell

@purpose: discover stores that are engaging with CB4 differently than
            the rest of their chain. Maybe this help explains low Hit Rate,
            of need for re-training.
"""

# import required libraries
import pandas as pd
import numpy as np
import pkg_resources
pkg_resources.require("matplotlib==3.1.0")
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import date

# define filepath of your response report, then click run
filepath = '/Users/michaelodonnell/plaid_pantry_RR_20210203.csv'

def cb4_corrplot(response_report):
    
    # read in csv as a dataframe
    df = pd.read_csv(filepath)
    
    # add necessary columns to response report to generate correlation plot
    df['recs'] = 1
    df['Hit'] = df['state'].apply(lambda x: 1 if x=='DONE' else 0)
    df['PresentationHit'] = df['reason_group_1'].apply(lambda x: 1 if x=='PRESENTATION' else 0)
    df['remark'] = df['remark'].fillna(0)
    df['FreeTextResponse'] = df['remark'].apply(lambda x: 0 if x == 0 else 1)
    df['AltActionResponse'] = df['reason_text_1'].apply(lambda x: 1 if x == 'Alternative action taken' else 0)

    # define key metrics by store:
        # Hit Rate, Response Rate, Free Text Rate, Presentation Rate
    # first, create an empty dataframe
    store_df = pd.DataFrame(columns = ['store_identifier',
                                       'Hit Rate',
                                       'Response Rate',
                                       'Free Text Rate',
                                       'Presentation Rate',
                                       'Alt Action Rate'])
    # next, generate a list of unique stores in your response report
    stores = df['store_identifier'].unique()
    #last, loop through each unique store and calculate each key metric (above)
    recs_analyzed = 0
    for s in stores:
        single_store_df = df[df['store_identifier'] == s]
        # don't count stores with less than 50 responses
        if sum(single_store_df['response_recieved']) < 50:
            continue
        temp_df = pd.DataFrame(data = {'store_identifier': s,
                                       'Hit Rate': round(sum(single_store_df['Hit'])/sum(single_store_df['response_recieved']), 3),
                                       'Response Rate': round(sum(single_store_df['response_recieved'])/sum(single_store_df['recs']), 3),
                                       'Free Text Rate': round(sum(single_store_df['FreeTextResponse'])/sum(single_store_df['response_recieved']), 3),
                                       'Presentation Rate': round(sum(single_store_df['PresentationHit'])/sum(single_store_df['Hit']), 3),
                                       'Alt Action Rate': round(sum(single_store_df['AltActionResponse'])/sum(single_store_df['Hit']), 3)},
                                        index=[0])
        # append temp_df to store_df
        store_df = store_df.append(temp_df)
        # count the total number of recs analyzed
        recs_analyzed += sum(single_store_df['recs'])
    
    # print Summary Statistics from this Analysis
    print("="*20)
    print("Analysis Summary:")
    print("-"*20)
    print("Stores analyzed:", len(store_df))
    print("Recs analyzed:", recs_analyzed)
    print("")
    
    # print Chain Metrics from this Analysis
    print("="*20)
    print("Chain Metrics:")
    print("-"*20)
    print("Response Rate:", round(sum(df['response_recieved'])/sum(df['recs']), 3)*100, "%")
    print("Hit Rate:", round(sum(df['Hit'])/sum(df['response_recieved']), 3)*100, "%")
    print("Presentation Rate:", round(sum(df['PresentationHit'])/sum(df['Hit']), 3)*100, "%")
    print("Free Text Rate:", round(sum(df['FreeTextResponse'])/sum(df['response_recieved']), 3)*100, "%")
    print("Alt Action Rate:", round(sum(df['AltActionResponse'])/sum(df['Hit']), 3)*100, "%")
    print("="*20)
    print("")
    
    # create corr plot
    store_df.sort_values(by=['Hit Rate'], inplace=True)
    corr = store_df.corr()
    corr[np.abs(corr)>.9] = 0   # set anything above .9 correlation to 0 (Hit Rate on itself)
    fig, ax = plt.subplots(figsize=(3, 6))
    colormap = sns.diverging_palette(220, 10, as_cmap = True)
    dropvals = np.zeros_like(corr)
    dropvals[np.triu_indices_from(dropvals)] = True
    sns.heatmap(corr[['Hit Rate']].sort_values(by=['Hit Rate'], ascending = False),
                cmap = colormap, vmax=1, vmin = -1,
                annot = True, fmt = ".2f", cbar_kws={"shrink": .6})
    plt.yticks(rotation=0, fontname = '.Keyboard')  # make y ticks horizontal
    plt.xticks(fontname = '.Keyboard') 
    plt.show()
    
    # export details as a csv
    today = date.today()
    store_df.to_csv("correlation_analysis"+str(today.strftime("%b-%d-%Y"))+".csv",
                    index=False)
    
cb4_corrplot(filepath)