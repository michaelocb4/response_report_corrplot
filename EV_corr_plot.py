#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28

@author: michaelodonnell

@purpose: discover stores that are engaging with CB4 differently than
            the rest of their chain. Maybe this help explains low Hit Rate,
            of need for re-training.
"""

# import required libraries
import pandas as pd
import numpy as np
import pkg_resources
pkg_resources.require("matplotlib==3.1.0")
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import date

# define filepaths of your response report and evaluation report, then click run
#response_report = '/Users/michaelodonnell/Levis_RR_20210203.csv'
#eval_report = '/Users/michaelodonnell/Levis_EV_20210203.csv'
response_report = '/Users/michaelodonnell/plaid_pantry_RR_20210203.csv'
eval_report = '/Users/michaelodonnell/plaid_pantry_EV_20210203.csv'

def cb4_revenue_corrplot(response_report = response_report,
                         eval_report = eval_report):
    
    # read in csvs as a dataframes
    rr_df = pd.read_csv(response_report)
    ev_df = pd.read_csv(eval_report)
    
    
    # add necessary columns to response report to generate correlation plot
    rr_df['recs'] = 1
    rr_df['Hit'] = rr_df['state'].apply(lambda x: 1 if x=='DONE' else 0)
    rr_df['PresentationHit'] = rr_df['reason_group_1'].apply(lambda x: 1 if x=='PRESENTATION' else 0)
    rr_df['remark'] = rr_df['remark'].fillna(0)
    rr_df['FreeTextResponse'] = rr_df['remark'].apply(lambda x: 0 if x == 0 else 1)
    rr_df['AltActionResponse'] = rr_df['reason_text_1'].apply(lambda x: 1 if x == 'Alternative action taken' else 0)
    
    # add necessary columns to evaluation report to generate correlation plot
    ev_df = ev_df[ev_df['ImplementationPeriodID'] == 1]
    ev_df['rec_id'] = ev_df['RecommendationId']
    
    # add revenue columns to response report
    df = pd.merge(rr_df, ev_df,
                  how = 'inner', on= 'rec_id')
    
    # define key metrics by store:
    # first, create an empty dataframe
    store_df = pd.DataFrame(columns = ['store_identifier',
                                       'Hit Rate',
                                       'Response Rate',
                                       'Success Rate',
                                       #'Revenue Increase',
                                       'Free Text Rate',
                                       'Presentation Rate',
                                       'Alt Action Rate'])
    # next, generate a list of unique stores in your response report
    stores = df['store_identifier'].unique()
    #last, loop through each unique store and calculate each key metric (above)
    recs_analyzed = 0
    for s in stores:
        single_store_df = df[df['store_identifier'] == s]
        # don't count stores with less than 50 responses
        if sum(single_store_df['response_recieved']) < 50:
            continue
        temp_df = pd.DataFrame(data = {'store_identifier': s,
                                       'Hit Rate': round(sum(single_store_df['Hit'])/sum(single_store_df['response_recieved']), 3),
                                       'Response Rate': round(sum(single_store_df['response_recieved'])/sum(single_store_df['recs'])-0.01, 3),
                                       'Success Rate': round(sum(single_store_df['Summable'])/sum(single_store_df['response_recieved']), 3),
                                       #'Revenue Increase': round((single_store_df[single_store_df['Summable']==1]['LiftIncrease']).mean(), 3),
                                       'Free Text Rate': round(sum(single_store_df['FreeTextResponse'])/sum(single_store_df['response_recieved']), 3),
                                       'Presentation Rate': round(sum(single_store_df['PresentationHit'])/sum(single_store_df['Hit']), 3),
                                       'Alt Action Rate': round(sum(single_store_df['AltActionResponse'])/sum(single_store_df['Hit']), 3)},
                                        index=[0])
        # append temp_df to store_df
        store_df = store_df.append(temp_df)
        # count the total number of recs analyzed
        recs_analyzed += sum(single_store_df['recs'])
    
    # print Summary Statistics from this Analysis
    print("="*20)
    print("Analysis Summary:")
    print("-"*20)
    print("Stores analyzed:", len(store_df))
    print("Recs analyzed:", recs_analyzed)
    print("")
    
    # print Chain Metrics from this Analysis
    print("="*20)
    print("Chain Metrics:")
    print("-"*20)
    print("Response Rate:", round(sum(df['response_recieved'])/sum(df['recs']), 3)*100, "%")
    print("Hit Rate:", round(sum(df['Hit'])/sum(df['response_recieved'])*100, 1), "%")
    print("Success Rate:", round(sum(df['Summable'])/sum(df['recs'])*100, 1), "%")
    print("Presentation Rate:", round(sum(df['PresentationHit'])/sum(df['Hit']), 3)*100, "%")
    print("Free Text Rate:", round(sum(df['FreeTextResponse'])/sum(df['response_recieved']), 3)*100, "%")
    print("Alt Action Rate:", round(sum(df['AltActionResponse'])/sum(df['Hit']), 3)*100, "%")
    print("="*20)
    print("")
    
    # create corr plot
    store_df.sort_values(by=['Success Rate'], inplace=True)
    corr = store_df.corr()
    corr[np.abs(corr)>.99] = 0   # set anything above .9 correlation to 0 (Hit Rate on itself)
    fig, ax = plt.subplots(figsize=(3, 6))
    colormap = sns.diverging_palette(220, 10, as_cmap = True)
    dropvals = np.zeros_like(corr)
    dropvals[np.triu_indices_from(dropvals)] = True
    sns.heatmap(corr[['Success Rate']].sort_values(by=['Success Rate'], ascending = False),
                cmap = colormap, vmax=1, vmin = -1,
                annot = True, fmt = ".2f", cbar_kws={"shrink": .6})
    plt.yticks(rotation=0, fontname = '.Keyboard')  # make y ticks horizontal
    plt.xticks(fontname = '.Keyboard') 
    plt.show()
    
    # export details as a csv
    today = date.today()
    store_df.to_csv("success_correlation_analysis_"+str(today.strftime("%b-%d-%Y"))+".csv",
                    index=False)
    return store_df
    
test = cb4_revenue_corrplot()